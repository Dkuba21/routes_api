from django.urls import path

from routesapp import views

app_name='api'
urlpatterns = [
    path('helthcheck/', views.HelthCheckView.as_view(), name='helthcheck'),
    path('me/routes/page<int:page>/', views.MyRoutesView.as_view()),
    path('me/route/', views.StoreNewUserRouteView.as_view()),
    path('route/from<int:point_from_id>/to<int:point_to_id>/',
         views.BuildNewRouteView.as_view()),
    path('routes/page<int:page>/', views.AllRoutesView.as_view()),
    path('route/<int:route_id>', views.RouteView.as_view()),
    path('points/page<int:page>/', views.PointsView.as_view()),
    path('<int:user_id>/routes/page<int:page>/', views.UserRoutesView.as_view()),
    path('report/', views.ReportView.as_view())
]
