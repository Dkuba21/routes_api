import json

from django.contrib.auth.models import User
from rest_framework.reverse import reverse
from rest_framework.status import HTTP_200_OK
from rest_framework.test import APIClient

import pytest

USERNAME = 'john'
USER_PASSWORD = 'js.sj'


@pytest.fixture
def api_client():

    User.objects.create_user(username=USERNAME, email='js@js.com',
                             password=USER_PASSWORD)
    client = APIClient(enforce_csrf_checks=True)

    response = client.post(reverse('auth:token'),
                           json.dumps({"username": USERNAME,
                                       "password": USER_PASSWORD}),
                           content_type='application/json')
    client.credentials(HTTP_AUTHORIZATION=f'Token {response.data["token"]}')

    return client


@pytest.mark.django_db
def test_me(api_client):
    """Get self user info test."""
    url = reverse('auth:me')
    response = api_client.get(url)
    data = response.data
    assert response.status_code == HTTP_200_OK
    assert data["username"] == USERNAME


@pytest.mark.django_db
def test_users(api_client):
    """Get all users test."""
    User.objects.create_user(username='Elton', email='elton@js.com',
                             password='1234')

    url = reverse('auth:users')
    response = api_client.get(url)
    data = response.data
    assert response.status_code == HTTP_200_OK
