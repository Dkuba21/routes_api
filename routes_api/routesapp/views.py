import coreschema
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import authentication, status, parsers, renderers
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.schemas import coreapi as coreapi_schema, ManualSchema, coreapi

from routesapp.serializers import RouteSerializer, PointSerializer, \
    CreateRouteSerializer

from django.utils.translation import gettext as _

from services.report.report_service import get_report_service
from services.route.route_builder import get_route_builder
from services.route.routes_storage import get_route_service

route_service = get_route_service()
route_builder = get_route_builder()
report_service = get_report_service()


class HelthCheckView(APIView):
    authentication_classes = []
    permission_classes = []

    @swagger_auto_schema(responses={
        "200": openapi.Response(
            description=_("API health check."),
        )
    })
    def get(self, request):
        return Response({'status': 'ok'})


class MyRoutesView(APIView):
    authentication_classes = [authentication.TokenAuthentication]
    page_size = 10

    @swagger_auto_schema(responses={
        "200": openapi.Response(
            description=_("Paginated current user routes."),
            schema=RouteSerializer,
        )
    })
    def get(self, request, page):
        """Get current user routes."""
        return route_service.get_user_routes_paginator(request,
                                                       page=page,
                                                       page_size=self.page_size)


class BuildNewRouteView(APIView):
    authentication_classes = [authentication.TokenAuthentication]

    @swagger_auto_schema(responses={
        "200": openapi.Response(
            description=_("New route has been built."),
            schema=PointSerializer,
        )
    })
    def get(self, request, point_from_id, point_to_id):
        """Get new route, which is built between two points."""
        route: dict = route_builder.get_new_route(point_from_id, point_to_id)

        return Response(route)


class StoreNewUserRouteView(APIView):
    authentication_classes = [authentication.TokenAuthentication]

    parser_classes = (parsers.FormParser, parsers.MultiPartParser, parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    serializer_class = CreateRouteSerializer

    if coreapi_schema.is_enabled():
        schema = ManualSchema(
            fields=[
                coreapi.Field(
                    name="username",
                    required=True,
                    location='form',
                    schema=coreschema.String(
                        title="Username",
                        description="Valid username for authentication",
                    ),
                ),
                coreapi.Field(
                    name="password",
                    required=True,
                    location='form',
                    schema=coreschema.String(
                        title="Password",
                        description="Valid password for authentication",
                    ),
                ),
            ],
            encoding="application/json",
        )

    @swagger_auto_schema(request_body=CreateRouteSerializer,
                         responses={
        "201": openapi.Response(
            description=_("New route has been stored."),
            schema=RouteSerializer,
        )
    })
    def post(self, request):
        """Store new route."""

        route_service.store_user_route(request.data.get('points', []),
                                       request.data.get('title', None),
                                       request.user.id)

        return Response(status=status.HTTP_201_CREATED)


class AllRoutesView(APIView):
    authentication_classes = [authentication.TokenAuthentication]
    page_size = 10

    @swagger_auto_schema(responses={
        "200": openapi.Response(
            description=_("Paginated all existing routes."),
            schema=RouteSerializer,
        )
    })
    def get(self, request, page):
        """Get all existing routes."""
        return route_service.get_all_routes_paginator(request, self.page_size,
                                                      page=page)


class ReportView(APIView):
    authentication_classes = [authentication.TokenAuthentication]
    page_size = 10

    @swagger_auto_schema(responses={
        "200": openapi.Response(
            description=_("Paginated all existing routes."),
            schema=RouteSerializer,
        )
    })
    def get(self, request):
        """Get all existing routes."""
        report_data = report_service.get_report()

        return Response(report_data, status=status.HTTP_200_OK)


class RouteView(APIView):
    authentication_classes = [authentication.TokenAuthentication]

    @swagger_auto_schema(responses={
        "200": openapi.Response(
            description=_("One route by given id."),
            schema=RouteSerializer,
        )
    })
    def get(self, request, route_id):
        """Get route by id."""
        query_set = route_service.get_routes_queryset(route_id)
        result = RouteSerializer(query_set)

        return Response(result.data, status=status.HTTP_200_OK)


class UserRoutesView(APIView):
    authentication_classes = [authentication.TokenAuthentication]
    page_size = 10

    @swagger_auto_schema(responses={
        "200": openapi.Response(
            description=_("Paginated routes of given user."),
            schema=RouteSerializer,
        )
    })
    def get(self, request, user_id, page):
        """Return routes of given user."""
        return route_service.get_user_routes_paginator(request, self.page_size,
                                                       user_id, page=page)


class PointsView(APIView):
    authentication_classes = [authentication.TokenAuthentication]
    page_size = 100

    @swagger_auto_schema(responses={
        "200": openapi.Response(
            description=_("Paginated all existing points ."),
            schema=RouteSerializer,
        )
    })
    def get(self, request, page):
        """Return routes of given user."""
        return route_service.get_point_paginator(request, self.page_size, page)
