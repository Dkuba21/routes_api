from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models

from django.utils.translation import gettext as _


class TimeStampedMixin(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Point(TimeStampedMixin):
    """Geo point."""

    title = models.CharField('Name', max_length=50, unique=True, db_index=True)

    latitude = models.FloatField('Latitude', validators=[MinValueValidator(-90.0),
                                                         MaxValueValidator(90.0)],
                                 null=False)
    longitude = models.FloatField('Longitude', validators=[MinValueValidator(-180.0),
                                                           MaxValueValidator(180.0)],
                                  null=False)

    def __str__(self):
        return self.title

    class Meta(object):
        verbose_name = _('geo point')


class Route(TimeStampedMixin):
    """Geo route."""

    title = models.CharField('Name', max_length=50, unique=True, db_index=True)

    owner = models.ForeignKey(User, on_delete=models.CASCADE,
                              verbose_name='owner')

    points = models.ManyToManyField(Point, related_name='routes',
                                    verbose_name='points', blank=True)

    def __str__(self):
        return self.title

    class Meta(object):
        verbose_name = _('geo route')
