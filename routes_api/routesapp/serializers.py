from django.contrib.auth.backends import UserModel
from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from routesapp.models import Route, Point


class UserSerializer(ModelSerializer):

    class Meta:
        model = User
        fields = ['username']

        extra_kwargs = {
            'username': {
                'validators': []
            }
        }


class PointSerializer(ModelSerializer):

    class Meta:
        model = Point
        fields = ['id', 'title', 'latitude', 'longitude']
        extra_kwargs = {
            'title': {
                'validators': []
            }
        }


class RouteSerializer(ModelSerializer):

    points = serializers.SlugRelatedField(
        many=True,
        read_only=True,
        slug_field='title'
    )

    owner = UserSerializer()

    class Meta:
        model = Route
        fields = ['id', 'title', 'owner', 'points']
        write_only_fields = ['points', ]

    def validate_points(self, points):
        if not points:
            raise serializers.ValidationError(
                "Route points hasn't been specified!")
        return points


class CreateRouteSerializer(ModelSerializer):

    points = PointSerializer(many=True,
                             allow_null=False)

    class Meta:
        model = Route
        fields = ['title', 'points']

    def validate_points(self, points):
        points.is_valid(raise_exception=True)
        return points


class RouteOwnerSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserModel
        fields = ("username", )
