# Generated by Django 4.0.2 on 2022-02-19 12:35

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('routesapp', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='point',
            name='latitude',
            field=models.FloatField(validators=[django.core.validators.MinValueValidator(-90.0), django.core.validators.MaxValueValidator(90.0)], verbose_name='Latitude'),
        ),
        migrations.AlterField(
            model_name='point',
            name='longitude',
            field=models.FloatField(validators=[django.core.validators.MinValueValidator(-180.0), django.core.validators.MaxValueValidator(180.0)], verbose_name='Longitude'),
        ),
    ]
