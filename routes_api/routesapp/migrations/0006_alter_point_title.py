# Generated by Django 4.0.2 on 2022-02-19 13:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('routesapp', '0005_alter_point_options_alter_route_options'),
    ]

    operations = [
        migrations.AlterField(
            model_name='point',
            name='title',
            field=models.CharField(max_length=50, unique=True, verbose_name='Name'),
        ),
    ]
