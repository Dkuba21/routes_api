import random

from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

from routes_api.routesapp.models import Route, Point

from faker import Faker
fake = Faker()

NUM_POINTS = 100000

USERS = [
    ('Vasya', 'dom12345'),
    ('Kolya', 'dom12345'),
    ('Ivan', 'dom12345'),
    ('Jack', 'dom12345')
]


class Command(BaseCommand):
    help = 'Create users and put routes data to database'

    def _create_user(self, user_name: str, password: str):
        User = get_user_model()
        User.objects.get_or_create(username=user_name, password=password)

    def _create_points(self):
        points_to_create = []
        p_names = []
        count = 0

        for p_num in range(NUM_POINTS):
            point_name = fake.address()[:50]
            if point_name in p_names:
                point_name = fake.address()[:10]

            p_names.append(point_name)

            points_to_create.append(Point(title=point_name,
                                          latitude=random.uniform(-90.0, 90.0),
                                          longitude=random.uniform(-180.0, 180.0)))
            count += 1
            if not count % 10000:
                Point.objects.bulk_create(points_to_create)
                print(f'{count} has been written in.')
                points_to_create = []

    def _create_route(self, route_name: str, username: str):
        """Create route with random points."""
        _start = random.randint(0, NUM_POINTS / 2)
        slice_start = random.randint(_start, _start + 100)
        slice_stop = random.randint(slice_start, slice_start + 10)

        points = Point.objects.all()[slice_start:slice_stop]

        User = get_user_model()
        user = User.objects.get(username=username)
        route, created = Route.objects.get_or_create(title=route_name[:50], owner=user)

        if created:
            for point in points:
                route.points.add(point)

    def handle(self, *args, **kwargs):
        # self._create_points()

        for username, password in USERS:
            self._create_user(username, password)

            for route_num in range(100):
                self._create_route(fake.name(), username=username)

