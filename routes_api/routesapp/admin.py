from django.contrib import admin

from routesapp.models import Point, Route


@admin.register(Point)
class PointAdmin(admin.ModelAdmin):
    search_fields = ('title',)


@admin.register(Route)
class RouteAdmin(admin.ModelAdmin):
    list_display = ('title', 'owner')
    autocomplete_fields = ('points',)
