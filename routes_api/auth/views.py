from django.contrib.auth import get_user_model, logout
from django.contrib.auth.models import User
from django.http import JsonResponse
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from django.utils.translation import gettext as _
from rest_framework import authentication, permissions, status
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.generics import CreateAPIView
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.views import APIView

from auth.serializers import Response201AuthTokenSerializer, CreateUserSerializer, \
    GetUserSerializer


class CustomAuthToken(ObtainAuthToken):
    authentication_classes = []
    permission_classes = (permissions.AllowAny,)

    @swagger_auto_schema(responses={
        "201": openapi.Response(
            description=_("User has got Token"),
            schema=Response201AuthTokenSerializer,
        )
    })
    def post(self, request, *args, **kwargs):
        """Get new token for the user."""
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        response201 = Response201AuthTokenSerializer(
            data={"token": token.key, "user_id": user.pk})
        response201.is_valid(raise_exception=True)
        return JsonResponse(response201.data)


class CreateUserView(CreateAPIView):
    authentication_classes = []
    permission_classes = (permissions.AllowAny,)

    model = get_user_model()
    serializer_class = CreateUserSerializer


class UserLogout(APIView):
    authentication_classes = [authentication.TokenAuthentication]

    def get(self, request):
        """Logout current user."""
        request.user.auth_token.delete()
        logout(request)
        return Response(status=status.HTTP_200_OK)


class UserView(APIView):
    authentication_classes = [authentication.TokenAuthentication]

    page_size = 10

    @swagger_auto_schema(responses={
        "200": openapi.Response(
            description=_("All users"),
            schema=GetUserSerializer,
        )
    })
    def get(self, request):
        """Get all users info."""
        users = User.objects.all()

        paginator = PageNumberPagination()
        paginator.page_size = self.page_size
        result_page = paginator.paginate_queryset(users, request)
        serializer = GetUserSerializer(result_page, many=True)

        return paginator.get_paginated_response(serializer.data)


class MeView(APIView):
    """
    Return the given dog.
    """
    authentication_classes = [authentication.TokenAuthentication]

    @swagger_auto_schema(
        responses={
        "200": openapi.Response(
            description=_("Current user"),
            schema=GetUserSerializer,
        )
    })
    def get(self, request):
        """Get curent user info."""
        user = User.objects.get(username=request.user.username)
        result = GetUserSerializer(user)

        return Response(result.data, status=status.HTTP_200_OK)
