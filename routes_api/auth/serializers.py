from django.contrib.auth import get_user_model
from rest_framework import serializers

UserModel = get_user_model()


class Response201AuthTokenSerializer(serializers.Serializer):
    token = serializers.CharField(required=True, allow_blank=False)
    user_id = serializers.IntegerField(required=True)


class GetUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserModel

        fields = ("id", "username")


class CreateUserSerializer(serializers.ModelSerializer):

    password = serializers.CharField(write_only=True)

    def create(self, validated_data):

        user = UserModel.objects.create_user(
            username=validated_data['username'],
            password=validated_data['password'],
        )

        return user

    class Meta:
        model = UserModel

        fields = ("id", "username", "password", )