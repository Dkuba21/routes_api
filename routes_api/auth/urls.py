from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token

from auth import views

app_name = 'auth'
urlpatterns = [
    path('token-auth/', obtain_auth_token, name='token'),
    path('register_user/', views.CreateUserView.as_view()),
    path('user_logout/', views.UserLogout.as_view()),
    path('users/', views.UserView.as_view(), name='users'),
    path('me/', views.MeView.as_view(), name='me')
]
