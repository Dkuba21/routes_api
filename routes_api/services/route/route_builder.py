from random import randint

from services.model.point import Point
from services.route.routes_storage import get_route_service


class NoPointsException(Exception):
    pass


class RouteBuilder:

    def __init__(self, route_service=get_route_service()):
        self.route_service = route_service

    def get_new_route(self, point_from_id, point_to_id) -> list[dict]:
        """
        Get new route between two points given by ids.
        """

        point_from: Point = self.route_service.get_point_by_id(point_from_id)
        point_to: Point = self.route_service.get_point_by_id(point_to_id)

        route: list[dict] = self.build_route(point_from, point_to)

        return route

    def build_route(self, point_from: Point, point_to: Point) -> list[dict]:
        """
        Dummy route building method.

        It just gets all points from DB and build new list with departure
        and destination points and random number of existing points between them.
        """
        all_points: list[Point] = self.route_service.get_all_points()

        if not all_points:
            raise NoPointsException('No any points available!')

        all_points.remove(point_from)
        all_points.remove(point_to)

        new_route: list[dict] = [point_from.dict(), ]

        max_index = len(all_points) - 1

        for index in range(randint(0, int(max_index/2)),
                           randint(int(max_index/2), max_index)):

            new_route.append(all_points[index].dict())

        new_route.append(point_to.dict())

        return new_route


def get_route_builder():
    return RouteBuilder()
