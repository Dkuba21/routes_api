"""Routes storage."""

from orjson import orjson

from django.contrib.auth.models import User

from rest_framework.pagination import PageNumberPagination, BasePagination
from rest_framework.renderers import JSONRenderer

from routesapp.models import Route, Point
from routesapp.serializers import RouteSerializer, PointSerializer

from services.model.point import Point as _Point


class RouteService:
    """Django ORM routes storage class."""

    def get_route_serialized(self):
        """Serialize route points data"""

    def get_point_by_id(self, point_id) -> _Point:
        """Get Point model by point id."""
        point_queryset = Point.objects.get(id=point_id)
        serializer = PointSerializer(point_queryset)
        point: dict = orjson.loads(JSONRenderer().render(serializer.data))

        return _Point.parse_obj(point)

    def get_all_points(self) -> list[_Point]:
        """
        Get all points from Django ORM and convert them to Pydantic model
        to pass them to outer service.
        """
        points_queryset = self.get_points_queryset()
        serializer = PointSerializer(points_queryset, many=True)
        points: list[dict] = orjson.loads(JSONRenderer().render(serializer.data))

        _points: list[_Point] = []

        for point in points:
            _point = _Point.parse_obj(point)
            _points.append(_point)

        return _points

    def get_user_routes_queryset(self, user_id):
        """Get routes by user id"""
        user = User.objects.get(id=user_id)
        user_routes = Route.objects.filter(owner=user).all()

        return user_routes

    def get_routes_queryset(self, route_id=None):
        """Get all routes queryset from Django ORM."""
        if not route_id:
            return Route.objects.all()
        else:
            return Route.objects.get(id=route_id)

    def get_points_queryset(self):
        """Get all points queryset from Django ORM."""
        return Point.objects.all()

    def get_point_paginator(self, request, page_size, page=1):
        points = self.get_points_queryset()
        return self._get_paginated_response(points, request, PointSerializer, page_size,
                                            page=page)

    def get_user_routes_paginator(self,
                                  request,
                                  page_size,
                                  page=1,
                                  user_id=None,
                                  ) -> BasePagination:

        if user_id:
            routes = self.get_user_routes_queryset(user_id)
        else:
            routes = self.get_user_routes_queryset(request.user.id)

        return self._get_paginated_response(routes, request, RouteSerializer, page_size,
                                            page=page)

    def get_all_routes_paginator(self, request, page_size, page=1):
        routes = self.get_routes_queryset()
        return self._get_paginated_response(routes, request, RouteSerializer, page_size,
                                            page=page)

    def _get_paginated_response(self, query_set, request, serializer, page_size, page=1):
        """Common method to get paginator with any type of serializer."""
        paginator = PageNumberPagination()
        paginator.page_size = page_size
        paginator.page = page
        result_page = paginator.paginate_queryset(query_set, request)

        serializer = serializer(result_page, many=True)

        return paginator.get_paginated_response(serializer.data)

    def store_user_route(self, points: list[dict], route_name: str, user_id: int):
        """
        Store new route in database.

        The route will be saved with given points, title and current user as owner.
        """

        points_serializer = PointSerializer(data=points, many=True)
        points_serializer.is_valid(raise_exception=True)

        user = User.objects.get(id=user_id)

        route_data = {'title': route_name, 'owner': {'username': user.username}}

        route_serializer = RouteSerializer(data=route_data)
        route_serializer.is_valid(raise_exception=True)

        new_points = []
        for point in points:
            obj, created = Point.objects.get_or_create(title=point['title'],
                                                       latitude=point['latitude'],
                                                       longitude=point['longitude'])
            new_points.append(obj)

        new_route: Route = Route.objects.create(owner=user, title=route_name)
        new_route.points.add(*new_points)


def get_route_service():
    return RouteService()
