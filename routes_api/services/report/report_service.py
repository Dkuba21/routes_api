from django.contrib.auth.models import User
from orjson import orjson
from rest_framework.renderers import JSONRenderer

from routesapp.serializers import PointSerializer
from services.geo.geo_service import get_geo_service
from services.model.point import Point as _Point


class ReportService:

    def __init__(self, geo_service=get_geo_service()):
        self.geo_service = geo_service

    def get_report(self) -> list[dict]:
        """Get routes report."""
        reports = []
        for user in User.objects.all():
            total_lenght: float = 0
            report = {'username': user.username, 'total_routes': user.route_set.count()}
            user_routes = user.route_set.all()

            for route in user_routes:
                points_query_set = route.points
                serializer = PointSerializer(points_query_set, many=True)
                points: list[dict] = orjson.loads(JSONRenderer().render(
                    serializer.data))

                if points:
                    _points: list[_Point] = []

                    for point in points:
                        _point = _Point.parse_obj(point)
                        _points.append(_point)

                    total_lenght += self.geo_service.get_length(_points)

            report.update({'total_lenght': total_lenght})

            reports.append(report)

        return reports


def get_report_service():
    return ReportService()
