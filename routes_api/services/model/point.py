from pydantic import BaseModel


class Point(BaseModel):
    """Point model to have and interface between services(modules)."""
    id: int
    title: str
    latitude: float
    longitude: float
