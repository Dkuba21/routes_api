import random

from services.model.point import Point


class GeoService:

    def get_length(self, point: list[Point]) -> float:
        """Some comprehensive GEO service get length handler."""

        # Dummy length calculation
        # ofcourse it's better to call google api fo that
        return random.uniform(1.0, 1000)


def get_geo_service():
    return GeoService()
