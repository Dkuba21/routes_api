## Install and Run


- Create .env file from .env.example  


- start postgres with commnad  
```docker run --name routes_db -e POSTGRES_PASSWORD=1 -d -p 5432:5432 postgres:13```  


- Create venv(I use python3.9) and install dependencies  
```pip install -r requirements.txt```  


- Apply Django migrations  
```python manage.py migrate --noinput```  


- Create Django superuser  
```python manage.py createsuperuser```    


- Collect static files
```python manage.py collectstatic```  


- Run Django app  
```python manage.py runserver```    


- Check if admin panel is available  
``http:\\127.0.0.1:8000\admin``

-Swagger UI is awailable on
``http:\\127.0.0.1:8000\swagger``


### TODO-list

- Write docker-compose file to up and build services in one command
- Write tests(!!!)
- Move autentification and geo services to separated micro-services and use nginx to single entry point

## Usage

Api is documented in Swagger UI.

Register user:  
```curl -X POST "http://127.0.0.1:8000/api/register_user/" -H  "accept: application/json" -H  "Content-Type: application/json" -d '{"username": "Serg21",  "password": "dom12345"}'```

Get auth token for user:  
```curl -X POST "http://127.0.0.1:8000/api/token-auth/" -H  "accept: application/json" -H  "Content-Type: application/json" -d '{"username": "Serg21",  "password": "dom12345"}'```

Api request with token auth example:

```curl -X GET "http://127.0.0.1:8000/api/me/" -H  "accept: application/json" -H  "Authorization: Token 20ad648baa5cf5cb5f131894330d38a73560841c"```

You can set token in Swagger UI as ```Token <given token>```.  
